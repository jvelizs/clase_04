const express = require('express')
const bodyParse = require('body-parser')
const { response } = require('express')

const app = express()

app.use(bodyParse.json())
app.use(bodyParse.urlencoded({
        extended: true
}))

app.get('/', function (request, response){
        response.send('hola mundo desde api rest')
})

app.get('/:num1/:num2', (request, response) =>{
    response.json({ result: sumValues(request.params.num1, request.params.num2) })

})
app.post('/', (request, response)=> {
    if(request.body.num1 && request.body.num2){
    response,status(200).json({result: multiply(request.body.num1, request.body.num2)})
    }
    else{
        response.status(404).json({error: 'Something is missing'})
    }

})
app.listen(3010, function(){
    console.log('Server is running in port 3010')
})

function sumValues(num1, num2){
    return Number(num1) + Number(num2)
}

function multiply(num1, num2){
    return Number(num1)*Number(num2)
}